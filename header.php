<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Educenter
 * @subpackage Education Xpert
 *
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> <?php educenter_html_tag_schema(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php 
	if ( function_exists( 'wp_body_open' ) ) {
		wp_body_open();
	} else {
		do_action( 'wp_body_open' );
	}
?>

<div id="page" class="site">

	<a class="skip-link screen-reader-text" href="#content">
		<?php esc_html_e( 'Skip to content', 'education-xpert' ); ?>
	</a>

	<header id="masthead" class="site-header general-header headerone" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
		<?php if(function_exists('educenter_top_header')); echo educenter_top_header(); ?>

		<div class="bottom-header">
			<div class="container">
				<div class="header-middle-inner">
					<div class="site-branding logo">
						
						<?php the_custom_logo(); ?>

						<div class="brandinglogo-wrap">
							<h1 class="site-title">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<?php bloginfo( 'name' ); ?>
								</a>
							</h1>
							<?php
								$description = get_bloginfo( 'description', 'display' );
								if ( $description || is_customize_preview() ) : ?>
									<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
							<?php endif;  ?>
						</div>

						<button class="header-nav-toggle" data-toggle-target=".header-mobile-menu"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
				            <div class="one"></div>
				            <div class="two"></div>
				            <div class="three"></div>
				        </button><!-- Mobile navbar toggler -->

					</div><!-- .site-branding -->
					
					
					<div class="contact-info">
					    <div class="quickcontact">
				        	<?php
								$quick_content = get_theme_mod('educenter_quick_content',  json_encode(array(
									array(
										'icon' => 'fas fa-envelope',
										'label' => esc_html('Email Address','education-xpert'),
										'val' => get_theme_mod('educenter_email_address', 'info@sptheme.com'),
										'link' => '',
										'enable' => 'on'
									),
									array(
										'icon' => 'fas fa-phone',
										'label' => esc_html('Phone Number','education-xpert'),
										'val' => get_theme_mod('educenter_phone_number', '+01-559-236-8009'),
										'link' => '',
										'enable' => 'on'
									),
									array(
										'icon' => 'fas fa-map',
										'label' => esc_html('Contact Address','education-xpert'),
										'val' => get_theme_mod('educenter_map_address', '28 Street, New York City'),
										'link' => '',
										'enable' => 'on'
									)
									
								)));

								$quick_content = json_decode( $quick_content );
								
								if( is_array( $quick_content ) ){
									foreach( $quick_content as $quick ){
										if( $quick->enable !== 'on' || $quick->val == '' ) continue; ?>


										<div class="get-tuch text-left">
											<?php if( $quick->icon ): ?>
												<i class="<?php echo esc_attr($quick->icon); ?>"></i>
											<?php endif; ?>
											<ul>
												<?php if( $quick->label ): ?>
												<li>
													<h4><?php echo esc_html($quick->label); ?></h4>
												</li>
												<?php endif; ?>

												<?php if( $quick->link || $quick->val ): ?>
												<li>
													<p>
														<?php if( $quick->link ): ?>
														<a href="tel:<?php echo esc_url( $quick->link ); ?>">
														<?php endif; ?>
															<?php echo esc_html( $quick->val ); ?>
														<?php if( $quick->link): ?>
														</a>
														<?php endif; ?>
													</p>
												</li>
												<?php endif; ?>
											</ul>
										</div>
									<?php }
								}; 
							?>
					    </div> <!--/ End Contact -->
					</div>
				</div>
			</div>
		</div>
		
		<div class="nav-menu">
			<div class="container">
				<div class="box-header-nav main-menu-wapper">
					<?php
						wp_nav_menu( array(
								'theme_location'  => 'menu-1',
								'menu'            => 'primary-menu',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'main-menu',
							)
						);
					?>
		        </div>
			</div>
		</div>
		
	</header><!-- #masthead -->

	<div id="content" class="site-content content">
		